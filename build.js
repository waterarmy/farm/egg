async function getExternalsFromPackageJson() {
  const packageText = await Bun.file("./package.json").text()
  const packageJson = JSON.parse(packageText);

  const sections = ["dependencies", "devDependencies", "peerDependencies"];
  const externals = new Set();

  for (const section of sections)
    if (packageJson[section])
      Object.keys(packageJson[section]).forEach((_) => externals.add(_));

  return Array.from(externals);
}

await Bun.build({
  entrypoints: ['./src/index.ts'],
  outdir: './out',
  external: await getExternalsFromPackageJson(),
})
