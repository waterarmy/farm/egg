export interface Message {
  success: boolean;
  message: string;
  data?: any;
}
