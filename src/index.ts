import { Elysia } from "elysia";
import { cors } from "@elysiajs/cors";
import { swagger } from "@elysiajs/swagger";
import { getElysiaConfig, getSweggerConfig } from "@/helpers";
import { userApp } from "@/modules/user";

const PORT = Number(Bun.env.PORT) | 3999;

const app = new Elysia(getElysiaConfig())
  .use(cors())
  .use(swagger(getSweggerConfig()))
  .get("/", ({ redirect }) => redirect("/swagger"))
  .use(userApp)
  .listen(PORT);

const { hostname, port } = app.server || { hostname: "localhost", port: PORT };

console.log(`Elysia is running at http://${hostname}:${port}`);
