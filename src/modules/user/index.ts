import { Elysia } from "elysia";
import { withDescription, withTags } from "@/helpers";
import { withUserDetailsValidator } from "./user.validators";
import { type Message } from "@/types";

export const userApp = new Elysia({
  prefix: "api/user",
  ...withTags(["user"]),
})
  .get("/sign-in", () => "Sign in", withDescription("用户登录"))
  .get("/sign-up", () => "Sign up", withDescription("用户注册"))
  .get("/profile", () => "Profile", withDescription("获取用户的个人信息"))
  .get(
    "/:id",
    ({ params: { id }, query: { from, theme } }): Message => {
      return {
        success: true,
        message: `Get user ${id}`,
        data: { id, from, theme },
      };
    },
    {
      ...withUserDetailsValidator,
      ...withDescription("获取用户基本信息，一般用于管理用户"),
    },
  );
