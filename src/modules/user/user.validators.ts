import { t } from "elysia";
import { createErrorSchema, createSuccessSchema } from "@/helpers";

export const withUserDetailsValidator = {
  params: t.Object({
    id: t.Numeric(),
  }),
  query: t.Object({
    from: t.Optional(t.String()),
    theme: t.Optional(t.String()),
  }),
  response: {
    200: createSuccessSchema(
      t.Object(
        {
          id: t.Number(),
          from: t.Optional(t.String()),
          theme: t.Optional(t.String()),
        },
        { description: "User data" },
      ),
    ),
    400: createErrorSchema(),
  },
};
