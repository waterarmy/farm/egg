import { type ElysiaConfig, t, type TSchema } from "elysia";
import { type ElysiaSwaggerConfig } from "@elysiajs/swagger";

export const getElysiaConfig = (): ElysiaConfig<"", false> => {
  return {};
};

export const getSweggerConfig = (): ElysiaSwaggerConfig => {
  return {
    documentation: {
      info: {
        title: "Egg Documentation",
        version: "1.0.0",
      },
    },
    exclude: ["/"],
  };
};

export const withTags = (tags: string[]) => {
  return {
    detail: {
      tags,
    },
  };
};

export const withDescription = (description: string) => {
  return {
    detail: {
      description,
    },
  };
};

export const createSuccessSchema = (data: TSchema) => {
  return t.Object({
    success: t.Boolean({ default: true, description: "Operation status" }),
    message: t.String({ description: "Success message" }),
    data: t.Optional(data),
  });
};

export const createErrorSchema = () => {
  return t.Object({
    success: t.Boolean({ default: false, description: "Operation status" }),
    message: t.String({ description: "Error message" }),
  });
};
